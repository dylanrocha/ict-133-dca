<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 25.11.2019
 * Time: 08:53
 */
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        * {
            box-sizing: border-box;
        }

        ul {
            list-style-type: none;
        }

        body {
            font-family: Verdana, sans-serif;
        }

        .month {
            padding: 70px 25px;
            width: 100%;
            background: #1abc9c;
            text-align: center;
        }

        .month ul {
            margin: 0;
            padding: 0;
        }

        .month ul li {
            color: white;
            font-size: 20px;
            text-transform: uppercase;
            letter-spacing: 3px;
        }

        .month .prev {
            float: left;
            padding-top: 10px;
        }

        .month .next {
            float: right;
            padding-top: 10px;
        }

        .weekdays {
            margin: 0;
            padding: 10px 0;
            background-color: #ddd;
        }

        .weekdays li {
            display: inline-block;
            width: 13.6%;
            color: #666;
            text-align: center;
        }

        .days {
            padding: 10px 0;
            background: #eee;
            margin: 0;
        }

        .days li {
            list-style-type: none;
            display: inline-block;
            width: 13.6%;
            text-align: center;
            margin-bottom: 5px;
            font-size: 12px;
            color: #777;
        }

        .days li .active {
            padding: 5px;
            background: #1abc9c;
            color: white !important
        }

        /* Add media queries for smaller screens */
        @media screen and (max-width: 720px) {
            .weekdays li, .days li {
                width: 13.1%;
            }
        }

        @media screen and (max-width: 420px) {
            .weekdays li, .days li {
                width: 12.5%;
            }

            .days li .active {
                padding: 2px;
            }
        }

        @media screen and (max-width: 290px) {
            .weekdays li, .days li {
                width: 12.2%;
            }
        }
    </style>
</head>
<body>
<form method="GET" action="calendar.php">
    <select name="choixMois">
        <option>January</option>
        <option>February</option>
        <option>March</option>
        <option>April</option>
        <option>May</option>
        <option>June</option>
        <option>Jully</option>
        <option>August</option>
        <option>September</option>
        <option>October</option>
        <option>November</option>
        <option>December</option>
    </select>
    <br>
    <input type="submit">
</form>
<?php

$currentMonth = date("F");
$currentYear = date("Y");
$daysWeek = date("D");
$daysWeekNumeric = date("N");

?>
<div class="month">
    <?php
    calendarHeader();
    ?>
</div>


<ul class="weekdays">
    <?php


    affichageDuJour()
    ?>
</ul>


<ul class="days">
    <?php

    afficheNumeroJour()

    ?>

    
    <?php
    function affichageMois($element)
    {
        echo $element;
        echo "<br>";
        echo "2020";
    }

    //fonction affiche l'en-tête du calendrier
    function calendarHeader(){
        $moisTableau = array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        $mois = @$_GET['choixMois'];

        if (isset($mois)) {
            for ($i = 0; $i < 12; $i++) {
                switch ($mois) {
                    case $moisTableau[$i]:
                        affichageMois($mois);
                        break;
                }
            }

        } else
            echo date("F") . "<br>" . "2020";
    }


    function afficheNumeroJour(){
        $currentDay = date("d");
        $lastDay = date("t");
        for ($w = 1; $w < 8 - date("N"); $w++) {
            echo "<li></li>";
        }

        for ($day = 1; $day <= $lastDay; $day++) {
            if ($day == $currentDay) {
                echo '<li><span class="active">' . $day . '</span></li>';
            } else {
                echo "<li>" . $day . "</li>";
            }
        }
    }

    function affichageDuJour(){

        //Tableau associatif des jours
        $tableauJour = array(
            'Lundi' => 'Lun',
            'Mardi' => 'Mar',
            'Mercredi' => 'Mer',
            'Jeudi' => 'Jeu',
            'Vendredi' => 'Ven',
            'Samedi' => 'Sam',
            'Dimanche' => 'Dim'
        );

        //Va chercher chaque ellement du tableau associatif : $tableauJour en créer a chaque fois un <li>
        foreach ($tableauJour as $element) {
            echo "<li>$element</li>"; //affichera $jours[0],etc…
        }
    }
    ?>
</ul>
</body>
</html>
