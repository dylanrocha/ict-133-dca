<?php
// Partie 1

echo date("l d F o\n");
echo date("M jS o\n");
echo date("j/n/y h:i a\n");
echo date("d M o, H:i:s \n");
echo date("D, d M o H:i:s O\n");

// Partie 2
$date = date_create('2001-01-01');
date_time_set($date, 14, 55, 24);

echo date_format($date, 'l d F o') . "\n";
echo date_format($date, 'M jS o') . "\n";
echo date_format($date, 'd/m/y h:i a') . "\n";
echo date_format($date, 'd M o, H:i:s') . "\n";
echo date_format($date, 'D, d M o H:i:s O') . "\n";

