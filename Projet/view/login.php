<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 06.01.2020
 * Time: 09:17
 */
ob_start();
$titre = 'Login';
?>
    <article>
        <form class="form" method="post" action="index.php?action=checkLogin">
            <div class="container">
                <h1>Connexion</h1>
                <?php
                // si error égal true alors une erreur sera affichée
                if (isset($_GET['error']) && $_GET['error'] == true) {
                    echo '<h5 style="color: red;">Login incorrect</h5>';
                }
                ?>
                <label for="userEmail"><b>Email</b></label>
                <input type="email" placeholder="Enter your Email address" name="email" required>
                <label for="userPsw"><b>Password</b></label>
                <input type="password" placeholder="Enter your password" name="password" required>
            </div>
            <div class="container">
                <input type="submit" value="Login" class="btn btn-primary">
                <button type="reset" class="btn btn-primary">Reset</button>
            </div>
        </form>
    </article>
<?php
$content = ob_get_clean();
require "gabarit.php";
