<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 20.01.2020
 * Time: 08:49
 */
ob_start();
$titre = 'Login';
?>
    <article>
        <form class="form" method="post" action="index.php?action=createAccount">
            <div class="container">
                <h1>Créer un compte</h1>
                <?php
                // si error égal true alors une erreur sera affichée
                if (isset($_GET['error']) && $_GET['error'] == true) {
                    echo '<h5 style="color: red;">Inscription incorrect</h5>';
                }
                ?>
                <label for="userEmail"><b>Email</b></label>
                <input type="email" placeholder="Enter your email address" name="email" required>
                <label for="userPsw"><b>Password</b></label>
                <input type="password" placeholder="Enter your password" name="password" required>
                <label for="userPsw"><b>Password check</b></label>
                <input type="password" placeholder="Re enter your password" name="password2" required>
            </div>
            <div class="container">
                <input type="submit" value="Register" class="btn btn-primary">
                <button type="reset" class="btn btn-primary">Reset</button>
            </div>
        </form>
    </article>
<?php
$content = ob_get_clean();
require "gabarit.php";


