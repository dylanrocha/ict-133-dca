<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 16.12.2019
 * Time: 14:41
 */
session_start();

require "model/model.php";
/*
 * Function to display the home page to the user
 */
function home()
{
    $_GET['action'] = 'home';
    require "view/home.php";
}
// fonctionne qui affiche la page de login
function login()
{
    $_GET['action'] = 'login';
    require "view/login.php";
}
// fonction pour que l'utilisateur puisse se déconnecter
function logout()
{
    $_SESSION = session_destroy();
    home();
}
// fonctionne qui fera le checking au login pour savoir si les identifiants sont correctes
function checkLoginFunction($form)
{
    if (checkLogin($form)) {
        home();
    } else {
        login();
    }
}
// affiche la page des produits
function produits()
{
    $_GET['action'] = "produits";

    $produit_content = snows();
    $_GET['snows'] =$produit_content;
    require "view/produits.php";
}
// fonctionne qui ouvre la page register
function register()
{
    $_GET['action'] = 'register';
    require "view/register.php";
}
// Fonction qui exécutera la fonction pour créer un compte dans le model
function createAccountFunction($userData)
{
    if (createAccount($userData)) {
        home();
    } else {
        register();
    }
}