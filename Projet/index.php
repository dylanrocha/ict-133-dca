<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 16.12.2019
 * Time: 14:24
 */

require "controler/controler.php";

// Switch qui va lire la valeur de l'action et exécuter la fonction dans le controler, si aucune valeur alors par défaut ça sera la fonction home
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'home' :
            home();
            break;
        case 'login' :
            login();
            break;
        case 'logout' :
            logout();
            break;
        case 'produits' :
            produits();
            break;
        case 'checkLogin' :
            checkLoginFunction($_POST);
            break;
        case 'createAccount' :
            createAccountFunction($_POST);
            break;
        case 'register' :
            register();
            break;
        default :
            home();
    }
} else {
    home();
}
