<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 06.01.2020
 * Time: 15:05
 */

// cette fonction va checker si le compte introduit sur la page login existe, si oui se connecte et sinon affiche une erreur et dois ré introduire
function checkLogin($form)
{
    if (isset($form['email']) && isset($form['password'])) {
        if (file_exists("database/users.json")) {
            $users = json_decode(file_get_contents("database/users.json"), true);
            foreach ($users as $user) {
                if ($user['email'] == $form['email'] && $user['password'] == $form['password']) {
                    $_GET['error'] = false;
                    $_SESSION['email'] = $form['email'];
                    return true;
                } else {
                    $_GET['error'] = true;
                }
            }
            if ($_GET['error'] = true) {
                return false;
            }
        }
    }
}

// fonctionne qui permet de se créer un compte, si celui-ci n'existe pas alors il sera créer et l'écrit dans un fichier json, cas contraire une erreur sera affichée
function createAccount($userData)
{
    if ($_POST['password'] == $_POST['password2']) {
        $_GET['error'] = false;
        $id = 0;
        $arrayData = [];
        if (file_exists("database/users.json")) {
            $users = json_decode(file_get_contents("database/users.json"), true);
            foreach ($users as $user) {
                if ($user['email'] == $userData['email']) {
                    $_GET['error'] = true;
                }
            }
            if ($_GET['error'] == false) {
                foreach ($users as $user) {
                    $id = $user['id'];
                    $arrayData['id'] = $user['id'];
                    $arrayData['password'] = $user['password'];
                    $arrayData['email'] = $user['email'];
                }
                $arrayData['id'] = $id + 1;
                $arrayData['password'] = $userData['password'];
                $arrayData['email'] = $userData['email'];
                array_push($users, $arrayData);
                file_put_contents('database/users.json', json_encode($users));
                $_SESSION['email'] = $userData['email'];
                return true;
            }
        }
    } else {
        $_GET['error'] = true;
        return false;
    }
}


//Fonction qui permet de lire le fichier json des snows et pasr la suite de les faire afficher sur une page
function snows()
{
    $current_file = file_get_contents("database/snows.json");
    $parsed_snowData = json_decode($current_file);
    $array_data[] = $parsed_snowData;
    return $array_data;
}

