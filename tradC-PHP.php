<?php

$value = 20;
$step = 3;
$i = 0;
// Partie 1
/*
for ($i = 0; $i < 10; $i++) {
    if ($value >= 30) {
        echo("le nombre vaut $value");
    } else {
        echo("nombre trop petit");
    }
    $value += $step;
}
return 0;
*/
// Partie 2

while ($i < 10){
    if ($value >= 30) {
        echo("le nombre vaut $value");
    } else {
        echo("nombre trop petit");
    }
    $value += $step;
    $i++;
}
return 0;